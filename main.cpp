#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

//bool debuging = false;
bool debuging = true;

int wymiar = 10;

int** stworzPlansze();
void wyswietlPlansze(int** plansza);
void wyswietlDwiePlansze(int** planszaJeden,int** planszaDwa);

void dodajStatek(int** plansza, int x, int y);
void graczDodawanieStatkow(int** plansza);
void komputerDodawanieStatkow(int** plansza);

void pobierzKordynatyXY(int &x, int &y);
bool czyJestWolnePole(int** plansza, int x,int y);
bool czyPoprawneKordynaty(int x,int y);

void graczTura(int** plansza);
void komputerTura(int** plansza);

void strzalWPlansze(int** plansza, int x, int y);
bool polePoTrafieniu(int** plansza,int x, int y);

bool czyPrzegral(int** plansza);
void usunPlansze(int** plansza);


void intro()
{
    cout<<" ______   _______ __________________ _        _______    _______          _________ _______  _______ "<<endl;
    cout<<"(  ___ \\ (  ___  )\\__   __/\\__   __/( \\      (  ____ \\  (  ____ \\|\\     /|\\__   __/(  ____ )(  ____ \\"<<endl;
    cout<<"| (   ) )| (   ) |   ) (      ) (   | (      | (    \\/  | (    \\/| )   ( |   ) (   | (    )|| (    \\/"<<endl;
    cout<<"| (__/ / | (___) |   | |      | |   | |      | (__      | (_____ | (___) |   | |   | (____)|| (_____ "<<endl;
    cout<<"|  __ (  |  ___  |   | |      | |   | |      |  __)     (_____  )|  ___  |   | |   |  _____)(_____  )"<<endl;
    cout<<"| (  \\ \\ | (   ) |   | |      | |   | |      | (              ) || (   ) |   | |   | (            ) |"<<endl;
    cout<<"| )___) )| )   ( |   | |      | |   | (____/\\| (____/\\  /\\____) || )   ( |___) (___| )      /\\____) |"<<endl;
    cout<<"|/ \\___/ |/     \\|   )_(      )_(   (_______/(_______/  \\_______)|/     \\|\\_______/|/       \\_______)"<<endl;

    cout<<endl<<endl;
    cout<<"Nacisnij Enter aby kontynuowac"<<endl;
    cin.sync();
    cin.get();
}

int main()
{
    srand (time(NULL));

    intro();

    int** graczPlansza = stworzPlansze();
    int** komputerPlansza = stworzPlansze();

    graczDodawanieStatkow(graczPlansza);
    komputerDodawanieStatkow(komputerPlansza);

    while(true)
    {
        wyswietlDwiePlansze(graczPlansza,komputerPlansza);
        cout<<endl<<"** Tura Gracza **"<<endl;
        graczTura(komputerPlansza);
        cout<<endl<<"** Tura Komputera **"<<endl;
        komputerTura(graczPlansza);

        if(czyPrzegral(graczPlansza))
           {
                cout<<"!!! Wygral Komputer !!!"<<endl;
                break;
           }
        else if (czyPrzegral(komputerPlansza))
            {
                cout<<"!!! Wygral Gracz !!!"<<endl;
                break;
            }
    }

    wyswietlDwiePlansze(graczPlansza, komputerPlansza);
    usunPlansze(komputerPlansza);
    usunPlansze(graczPlansza);

    cout<<endl<<"Nacisnij Enter aby zakonczyc"<<endl;
    cin.sync();
    cin.get();

    return 0;
}

int** stworzPlansze()
{
    int** plansza = new int * [wymiar];
    for (int i = 0; i<wymiar; i++) plansza[i] = new int [wymiar];
    for(int i=0;i<wymiar;i++)
        for(int j=0;j<wymiar;j++)
            plansza[i][j] = 0;
    return plansza;
}


void wyswietlPlansze(int** plansza)
{
    system("cls");
    //for(int i=0;i<256;i++)cout<<i<<(char)i<<endl;
    cout<<(char)201;for(int i=0;i<30;i++)cout<<(char)205;cout<<(char)187<<endl;
    cout<<(char)186<<"       Plansza Gracza         "<<(char)186<<endl;
    cout<<(char)204;for(int i=0;i<30;i++)cout<<(char)205;cout<<(char)185<<endl;
    cout<<(char)186<<" 1  2  3  4  5  6  7  8  9  10"<<(char)186<<endl;
    cout<<(char)204;for(int i=0;i<30;i++)cout<<(char)205;cout<<(char)185<<endl;

    for(int i=0;i<wymiar;i++)
    {
        cout<<(char)186;
        for(int j=0;j<wymiar;j++)
        {
            if (plansza[i][j] == 2) cout<<" X ";
            else if (plansza[i][j] == 1) cout<<" "<<(char)219<<" ";
            else cout<<"   ";
        }
        cout<<(char)186<<"    "<<(char)('A'+i)<<endl;
    }
    cout<<(char)200;for(int i=0;i<30;i++)cout<<(char)205;cout<<(char)188<<endl;
}

void wyswietlDwiePlansze(int** graczPlansza, int** komputerPlansza)
{
    system("cls");
    cout<<(char)201;for(int i=0;i<30;i++)cout<<(char)205;cout<<(char)187<<"         "<<(char)201;for(int i=0;i<30;i++)cout<<(char)205;cout<<(char)187<<endl;
    cout<<(char)186<<"       Plansza Gracza         "<<(char)186<<"         "<<(char)186<<"       Plansza Komputera      "<<(char)186<<endl;
    cout<<(char)204;for(int i=0;i<30;i++)cout<<(char)205;cout<<(char)185<<"         "<<(char)204;for(int i=0;i<30;i++)cout<<(char)205;cout<<(char)185<<endl;
    cout<<(char)186<<" 1  2  3  4  5  6  7  8  9  10"<<(char)186<<"         "<<(char)186<<" 1  2  3  4  5  6  7  8  9  10"<<(char)186<<endl;
    cout<<(char)204;for(int i=0;i<30;i++)cout<<(char)205;cout<<(char)185<<"         "<<(char)204;for(int i=0;i<30;i++)cout<<(char)205;cout<<(char)185<<endl;
    for(int i=0;i<wymiar;i++)
    {
        cout<<(char)186;
        for(int j=0;j<wymiar;j++)
        {
            if (graczPlansza[i][j] == 2) cout<<" X ";
            else if (graczPlansza[i][j] == 2) cout<<" "<<(char)219<<" ";
            else if (graczPlansza[i][j] == 3) cout<<" # ";
            else cout<<"   ";
        }
        cout<<(char)186<<"    "<<(char)('A'+i)<<"    "<<(char)186;
        for(int j=0;j<wymiar;j++)
        {
            if (komputerPlansza[i][j] == 2) cout<<" X ";
            else if (komputerPlansza[i][j] == 3) cout<<" # ";
            else if (komputerPlansza[i][j] == 1 && debuging) cout<<" "<<(char)219<<" ";
            else cout<<"   ";
        }
        cout<<(char)186<<endl;
    }
    cout<<(char)200;for(int i=0;i<30;i++)cout<<(char)205;cout<<(char)188<<"         "<<(char)200;for(int i=0;i<30;i++)cout<<(char)205;cout<<(char)188<<endl;
}

void dodajStatek(int** plansza, int x, int y)
{
    plansza[x-1][y-1] = 1;
}

void graczDodawanieStatkow(int** plansza)
{
    int i =0;
    int x,y;
    while(i<4)
    {
        wyswietlPlansze(plansza);
        cout<<"Podaj kordynaty dla statku "<<i<<endl;
        pobierzKordynatyXY(x, y);
        if (czyJestWolnePole(plansza,x,y))
        {
            dodajStatek(plansza,x,y);
            i++;
        }
        else
        {
            cout<<"Pozycja jest zajeta."<<endl;
            cout<<endl<<"Nacisnij Enter aby kontynuowac"<<endl;
            cin.sync();
            cin.get();
        }
    }
}

void komputerDodawanieStatkow(int** plansza)
{
    int i=0;
    while(i<4)
    {
        int x = rand()%10+1;
        int y = rand()%10+1;
        if (czyJestWolnePole(plansza,x,y))
        {
            dodajStatek(plansza,x,y);
            i++;
        }
    }
}

void pobierzKordynatyXY(int &x, int &y)
{
    int a,b;
    char tekst[10];
    while(true)
    {
        cin>>tekst;
        for(int i=0;i<3;i++) tekst[i] = toupper(tekst[i]);
        if (tekst[0]>= 'A' && tekst[0]<= 'J' && tekst[1] > '0' && tekst[1] <= '9')
        {
            a = (int)(tekst[0] - 'A'+1);
            if (tekst[1] == '1' && tekst[2] == '0') b = 10;
            else b = (int)(tekst[1] - '0');
        }
        else if (tekst[0] == '1' && tekst[1] == '0' && tekst[2]>= 'A' && tekst[2]<= 'J')
        {
            a = tekst[2] - 'A'+1;
            b = 10;
        }
        else if (tekst[0] > '0' && tekst[0] <= '9' && tekst[1]>= 'A' && tekst[1]<= 'J')
        {
            a = tekst[1] - 'A'+1;
            b = tekst[0] - '0';
        }
        else if (tekst[1] > '0' && tekst[1] <= '9' && tekst[0]>= 'A' && tekst[0]<= 'J')
        {
            b = tekst[1] - '0';
            a = tekst[0] - 'A'+1;
        }
        else
        {
            cout<<"Podaj poprawne kordynaty(np C4 lub 7B)"<<endl;
            continue;
        }
        break;
    }
    x = a;
    y = b;
}

bool czyJestWolnePole(int** plansza, int x,int y)
{
    return plansza[x-1][y-1] != 1;
}

bool czyPoprawneKordynaty(int x,int y)
{
    return x<=10 && x>0 && y<=10 && y>0;
}

void graczTura(int** plansza)
{
        int x,y;

        while(true)
        {
            pobierzKordynatyXY(x, y);
            polePoTrafieniu(plansza,x,y);
            if (!polePoTrafieniu(plansza,x,y)) break;
            else cout<<"W to pole juz strzelales"<<endl;
        }
        strzalWPlansze(plansza,x,y);
}

void komputerTura(int** plansza)
{
    while(true)
    {
        int x = rand()%10+1;
        int y = rand()%10+1;
        if (!polePoTrafieniu(plansza,x,y))
        {
            cout<<(char)(x+'A'+1)<<y<<endl;
            strzalWPlansze(plansza,x,y);
            break;
        }
    }
    cout<<endl<<"Nacisnij Enter aby kontynuowac"<<endl;
    cin.sync();
    cin.get();
}

void strzalWPlansze(int** plansza, int x, int y)
{
    if (plansza[x-1][y-1] == 1)
    {
        cout<<"Trafienie statku"<<endl;
        plansza[x-1][y-1] = 3;
    }
    else
    {
        cout<<"Pudlo"<<endl;
        plansza[x-1][y-1] = 2;
    }
}

bool polePoTrafieniu(int** plansza,int x, int y)
{
    return plansza[x-1][y-1] == 2 || plansza[x-1][y-1] == 3;
}

bool czyPrzegral(int** plansza)
{
    int wygrana = true;
    for(int i=0;i<wymiar;i++)
        for(int j=0;j<wymiar;j++)
            if (plansza[i][j] == 1)
                wygrana = false;
    return wygrana;
}

void usunPlansze(int** plansza)
{
    for (int i =0; i< wymiar; i++) delete[] plansza[i];
    delete[] plansza;
}

/*void wyswietlDwiePlansze(int** graczPlansza, int** komputerPlansza)
{
    system("cls");
    cout<<"       Plansza Gracza                             Plansza Komputera      "<<endl;
    cout<<"| 1  2  3  4  5  6  7  8  9  10|         | 1  2  3  4  5  6  7  8  9  10|"<<endl;
    cout<<"--------------------------------         --------------------------------"<<endl;
    for(int i=0;i<wymiar;i++)
    {
        cout<<"|";
        for(int j=0;j<wymiar;j++)
        {
            if (graczPlansza[i][j] == 2) cout<<" X ";
            else if (graczPlansza[i][j] == 1) cout<<" "<<(char)219<<" ";
            else cout<<"   ";
        }
        cout<<"|    "<<(char)('A'+i)<<"    |";
        for(int j=0;j<wymiar;j++)
        {
            if (komputerPlansza[i][j] == 2) cout<<" X ";
            else if (komputerPlansza[i][j] == 1) cout<<" "<<(char)219<<" ";
            else cout<<"   ";
        }
        cout<<"|"<<endl;
    }
    cout<<"--------------------------------         --------------------------------"<<endl;
}*/

/*void wyswietlPlansze(int** plansza)
{
    system("cls");
    for(int i=0;i<256;i++)cout<<i<<(char)i<<endl;
    cout<<"       Plansza Gracza           "<<endl;
    cout<<"| 1  2  3  4  5  6  7  8  9  10|"<<endl;
    cout<<"---------------------------------"<<endl;
    for(int i=0;i<wymiar;i++)
    {
        cout<<"|";
        for(int j=0;j<wymiar;j++)
        {
            if (plansza[i][j] == 2) cout<<" X ";
            else if (plansza[i][j] == 1) cout<<" "<<(char)219<<" ";
            else cout<<"   ";
        }
        cout<<"|    "<<(char)('A'+i)<<endl;
    }
    cout<<"--------------------------------"<<endl;
}*/
